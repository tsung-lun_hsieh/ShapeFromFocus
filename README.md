# Shape from focus [GNU Octave]

Implementation of shape from focus (SFF) for GNU Octave. The codes were ported from matlab code (Version 1.5 by Said Pertuz https://www.mathworks.com/matlabcentral/fileexchange/55103-shape-from-focus).

This function estimates a depthmap and a reliability measure from defocused image sequences using shape-from-focus [1]. According to [2], depth for pixels with a reliability measure below 20 dB is not reliable and should be descarded. This code has been used in [2] and [3].

References
<br>[1] S.K. Nayar and Y. Nakagawa, IEEE Trans. Patt Anal. Mach. Intell. 16(8):824-831, 1994.
<br>[2] S. Pertuz, D. Puig, M. A. Garcia, Reliability measure for shape-from focus, Image Vis. Comput. 31:725-734, 2013.
<br>[3] S. Pertuz, D. Puig, M. A. Garcia, Analysis of focus measure operators for shape-from-focus, Pattern Recognition. 46(5): 1415-1432, 2013.

<br><br>Require Octave package "image" (https://gnu-octave.github.io/packages/image/).

For a demo: 
<br><br><code>octave ./sffdemo.m</code><br>
<br>./demo_IMG is directory of image sequences
<br>./demo_INF contains focus position files (single line CSV file; if lack of this file, default is iteration of number of images)
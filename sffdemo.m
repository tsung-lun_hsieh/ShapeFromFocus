clear, clc, close all
%% Load the variables of the focus sequence and ROI:
INF_Path = "./demo_INF";
imdata.focus = csvread(strjoin({INF_Path, "focus.csv"}, "/"));
imdata.ROI = csvread(strjoin({INF_Path, "ROI.csv"}, "/"));
%% Load image sequence:
IMG_Path = "./demo_IMG";
IMG_List = dir(fullfile(IMG_Path, '*.tif'));
imdata.images = fullfile({IMG_List.folder}, {IMG_List.name});

% This loads a structure with two fields: images, a 1x30 cell
% array where each cell is a string with the path of one
% frame of the focus sequence; and focus, a 1x30 vector with
% the focus position (in meters) corresponding to each frame
% of the focus sequence. This sequence was generated using:
% http://www.mathworks.com/matlabcentral/fileexchange/55095-defocus-simulation

%% Preview the image sequence:
%showimages(imdata.images, imdata.focus, imdata.ROI);

%% Perform SFF using 3-point gaussian interpolation
% as originally proposed by [1] and compute
% reliability according to [2]:

[z, r] = sff(imdata.images, 0, "glvm", imdata.focus, true, 9, []);

%% Carve depthmap by removing pixels with R<20 dB:
zc = z;
zc(r<20) = nan;

%% Display the result:
f = figure(1);
close(gcf), figure
subplot(1,2,1)
surf(z), shading flat, colormap copper
%% Shrink image to decrease resoultion and shading by interpolation to make surface smoother
%z1 = imresize(z, 0.1); surf(z1), shading interp, colormap copper
set(gca, 'zdir', 'reverse', 'xtick', [], 'ytick', [])
axis tight, grid off, box on
zlabel('pixel depth (mm)')
title('Full depthmap')

subplot(1,2,2), surf(zc), shading flat, colormap copper
set(gca, 'zdir', 'reverse', 'xtick', [], 'ytick', [])
axis tight, grid off, box on
zlabel('pixel depth (mm)')
title('Carved depthmap (R<20dB)')

waitfor(f)

% References:
% [1] S.K. Nayar and Y. Nakagawa, PAMI, 16(8):824-831, 1994. 
% [2] S. Pertuz, D. Puig, M. A. Garcia, Reliability measure for shape-from
% focus, IMAVIS, 31:725-734, 2013.
